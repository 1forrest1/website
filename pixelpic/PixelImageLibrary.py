from PIL import Image, UnidentifiedImageError
import numpy as np
import os
from pixelpic import PixelImage, Utilities as u
import time


class PixelImageLibrary(object):
    image_library = []
    images_in_folder = 0
    failCount = 0
    usable_images_count = 0
    pid = None

    def __init__(self, folder_name=None):
        if folder_name is not None:
            self.add_folder_to_library(folder_name)

    def add_folder_to_library(self, folder_name):
        print(os.listdir())
        print(os.listdir(folder_name))
        start = time.time()
        for file in os.listdir(folder_name):
            image = PixelImage.PixelImage(folder_name+"/"+file)
            if image.is_usable():
                self.image_library.append(image)
                self.usable_images_count += 1
            else:
                self.failCount += 1
        end = time.time()
        print("folder added in", round(end - start, 2), "seconds")

    def resize_library(self, pid):
        start = time.time()
        for i in range(len(self.image_library)):
            self.image_library[i].resize(pid)
        end = time.time()
        print("library resized in", round(end - start, 2), "seconds")

    def get_pixel_image(self, index):
        return self.image_library[index]

    def get_length(self):
        return len(self.image_library)

    def remove_image(self, index):
        self.image_library.pop(index)

