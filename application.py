from flask import Flask, flash, request, redirect, url_for, send_from_directory, render_template
from werkzeug.utils import secure_filename
from pixelpic import PixelPic, TemplateImage, PixelImageLibrary, Utilities as u
import os
from PIL import Image

app = Flask(__name__)
UPLOAD_FOLDER = 'uploads'
OUTPUT_FOLDER = 'output'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'ppm'}
template_name = "template_image.png"
selected_folders = []

application = app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['OUTPUT_FOLDER'] = OUTPUT_FOLDER


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


@app.route('/PixelPic', methods=['GET', 'POST'])
def upload_template_image():
    dir = os.listdir(UPLOAD_FOLDER)
    print("dir before", dir)

    for file in dir:
        os.remove(UPLOAD_FOLDER + "/" + file)
    print("dir after", dir)
    if request.method == 'POST':
        usable_file = False

        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            os.rename(UPLOAD_FOLDER + "/" + filename, UPLOAD_FOLDER + "/" + template_name)
            usable_file = True
            return redirect(url_for('upload_folder'))
    return render_template("pixel_pic/template_image_upload.html")


@app.route('/test', methods=['GET', 'POST'])
def display_test():
    if request.method == 'POST':
        usable_folder = False

        # check if the post request has the file part
        if 'folder' not in request.files:
            flash('No folder part')
            return redirect(request.url)
        folder = request.files.getlist("folder")
        # if user does not select file, browser also
        # submit an empty part without filename
        if len(folder) == 0:
            flash('No selected folder')
            return redirect(request.url)
        for file in folder:
            if file and allowed_file(file.filename):
                usable_folder = True
                folder_filename = secure_filename(file.filename.split('/')[-1])
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], folder_filename))
        if usable_folder:
            return redirect(url_for('generate_pixelPic'))
    return render_template("pixel_pic/image_folder_upload.html")


@app.route('/building,')
def generate_pixelPic():
    template_image = TemplateImage.TemplateImage(UPLOAD_FOLDER + "/" + template_name, images_per_side_string="m")
    #os.remove(UPLOAD_FOLDER + "/" + template_name)
    pixel_image_library = PixelImageLibrary.PixelImageLibrary()
    for folder in selected_folders:
        pixel_image_library.add_folder_to_library("static/"+folder)

    pixel_pic = PixelPic.PixelPic(template_image, pixel_image_library, size_m=1, repeating_images=True)
    pixel_pic.buildMasterImage()
    pil_pixel_pic = Image.fromarray(pixel_pic.get_overlay_image())
    pil_pixel_pic.save("static/pixelPic.png")
    return redirect(url_for('display_pixelPic'))
    # return redirect(url_for('uploaded_file', filename="/pixelPic.png"))


@app.route('/display')
def display_pixelPic():
    return render_template("pixel_pic/DisplayPixelPic.html")


#
# @app.route('/PixelPic')
# def PixelPic():
#     return render_template("Full_JS_PixelPic/PixelPic.html")


@app.route('/Sudoku')
def display_sudoku():
    return render_template("sudoku.html")


@app.route('/Backgammon')
def display_backgammon():
    return render_template("Backgammon.html")


@app.route('/winner')
def display_winner():
    return render_template("Winner.html")


@app.route('/looser')
def display_looser():
    return render_template("looser.html")



@app.route('/')
@app.route('/About')
def display_forrest_wargo():
    return render_template("Forrest_Wargo.html")


@app.route('/uploads/<filename>')
def uploaded_file(filename):
    return send_from_directory(app.config['OUTPUT_FOLDER'], filename)

@app.route('/folder_upload', methods=['GET', 'POST'])
def upload_folder():
    if request.method == 'POST':
        selected_folders.clear()
        if request.form.get("switch_0") == "on":
            selected_folders.append("Mountains")
        if request.form.get("switch_1") == "on":
            selected_folders.append("Winter")
        if request.form.get("switch_2")== "on":
            selected_folders.append("movie_posters")
        if request.form.get("switch_3") == "on":
            selected_folders.append("volcanic")
        print(selected_folders)
        return redirect(url_for('generate_pixelPic'))
        #return render_template("pixel_pic/folder_selection.html")

    return render_template("pixel_pic/folder_selection.html")


if __name__ == '__main__':
    app.run(debug=True)

